# Kunstgeschichte als Datenwissenschaft

„Kunstgeschichte als Datenwissenschaft“ ist eine interdisziplinäre Lehrveranstaltung, die Informatik und Kunstgeschichte zusammenbringt. Sie ist für Masterstudierende der Informatik ein Praktikum und für Bachelor- oder Masterstudierende der Kunstgeschichte ein Seminar.

In der Lehrveranstaltung sollen Verfahren untersucht werden, mit denen die kunsthistorische Erschließung und Analyse unterstützt werden kann. Im ersten Fall wird eine bislang nur gedruckt vorliegende Gesamtdarstellung der christlichen Ikonographie zur Entwicklung eines interaktiven Identifikationssystems verwendet. Im zweiten Fall wird der Versuch unternommen, auf automatisiertem Wege gängige Kunstwerktitel so zu erweitern, dass sie in einem regulären Thesaurus eingesetzt werden können. Im dritten Fall geht es um ästhetisch relevante Beschreibungsbegriffe und ihr Verhältnis zu geläufigen Einordnungen. Und im vierten Fall wollen wir versuchen, aus dem großen Bestand von schon digitalisierten historischen Druckbeständen kunsthistorische Daten zu extrahieren.


## Thema 1

[Iconclass](http://iconclass.org/) ist ein ikonographisches Klassifizierungssystem, das die europäische, vor allem christliche Kunst feingranular nach Themenzugehörigkeiten einzuteilen erlaubt. Es wird bei der Inventarisierung von vielen europäischen Museen eingesetzt (z. B. vom [Städel Museum Frankfurt](https://sammlung.staedelmuseum.de/de)). Der Großteil der musealen Objekte bleibt aber unklassifiziert, womit auch der Einsatz von Iconclass zu einer computergestützten Automatisierung von Identifikationsprozessen behindert wird. Es soll im Seminar untersucht werden, ob und welche Möglichkeiten es gibt, generische Titeldaten von Kunstwerken, wie sie in fast allen Bildsammlungen vorhanden sind, automatisch auf Iconclass-Notationen zu mappen, sodass die Menge der in diesem Klassifizierungssystem vorliegenden Daten entschieden gesteigert werden kann.

* **Daten**
Ein *Dump* des [Iconclass](http://iconclass.org/)-Systems wird zur Verfügung gestellt. Weiterhin liegen über 500.000 Kunstwerke inklusive Metadaten vor, die mit Iconclass annotiert worden sind.


## Thema 2

[ARTigo](http://www.artigo.org/) ist zweierlei: erstens eine Internetplattform, in der einem auf seine Qualifikation hin nicht näher befragten Publikum digitale Reproduktionen von Kunstwerken vorgelegt werden, um diese in einem spielerisch-kompetitiven Verfahren zu annotieren. Zweitens eine semantische Suchmaschine, die auf Basis der über die *Crowd* generierten Annotationen (*Tags*) großer Bildmengen Herr wird, ohne auf die Hilfe von Spezialisten setzen zu müssen. Seit 2007 wurden über dieses „Ökosystem“ 9,6 Millionen deutsch-, englisch- und französischsprachige Taggings für über 55.000 Kunstwerke gesammelt. Dabei ist die Natur der Annotationen sehr disparat, weil keinerlei Vorgaben zu deren Qualität gemacht wurden. Im Seminar sollen ästhetisch relevante Begriffe (bspw. durch Franz Dornseiffs „Wortschatz nach Sachgruppen“) identifiziert und auf ihre heuristische Bedeutung für die Einordnung der Kunstwerke hin untersucht, d. h. Qualitäten von bestimmten Stillagen festgestellt werden.

* **Daten**
Ein bereinigter *Dump* der [ARTigo](http://www.artigo.org/)-Datenbank mit 9.669.410 Taggings wird unter [Open Data LMU](https://data.ub.uni-muenchen.de/136/) zur Verfügung gestellt.


## Thema 3

Das Lexikon der christlichen Ikonographie (LCI) in acht Bänden ist eines der großen Gemeinschaftsunternehmen der deutschen Nachkriegskunstgeschichte. Es ist eingeteilt in vier Bände zu den Heiligen und vier weitere zur allgemeinen christlichen Ikonographie. Die Informationsdichte in diesem Lexikon ist extrem hoch, eigentlich ist es aufgrund der vielen Abkürzungen kaum lesbar. Hinzu kommt ein systematisches Problem des Publikationstypus Lexikon, dass vielfach zuammengehörende Informationen über gewöhnlich weit auseinanderliegende Lemmata verteilt sind. Wir wollen uns im Seminar die Bände zur Heiligenikonographie vornehmen und einen *Chatbot* entwickeln, der die Identifikation der Heiligen aufgrund von am Ort (also z. B. in einer Kirche) beobachteten Eigenschaften ermöglicht. 

* **Daten**
Neben hochauflösenden Scans wird eine bereits mit *OCR* behandelte Version des Lexikons der christlichen Ikonographie (LCI) zur Verfügung gestellt.


## Thema 4

Insbesondere in weiter zurückliegenden Epochen sind große Mengen von Kunstwerken entstanden, die heute aus verschiedenen Gründen (z. B. Kriegseinwirkung oder Vernachlässigung) nicht mehr existieren, aber für eine umfassende Historisierung bedeutend sind. Die [Universitätsbibliothek Heidelberg](https://www.ub.uni-heidelberg.de/) hat mit ihrem Sondersammelgebiet Kunstgeschichte in den letzten Jahren einen guten Teil der Kunst-Fachzeitschriften des späten 19. und frühen 20. Jahrhunderts digitalisiert. In einigen dieser Zeitschriften sind Unmengen von Werken der zeitgenössischen Kunst abgebildet, die ansonsten historisch häufig nicht überliefert sind. Um diesen Datenbestand in die Forschung einzuführen, sollen diese Reproduktionen identifiziert, automatisch extrahiert und mit dem im Umfeld der Abbildung gegebenen Metadaten verbunden werden.

* **Daten**
Es werden 4.462 *Sandwich-PDFs* von 299 Fachzeitschriften zur Verfügung gestellt.


## Vorkenntnisse

Je nach Thema: Überblickskenntnisse über die frühneuzeitliche Kunst- und Kunsttheoriegeschichte, Kenntnisse der Kunstgeschichte des 19. Jahrhunderts.


## Weitere Informationen

Es ist geplant, dass jedes Thema in einer Gruppe bearbeitet wird, die sich jeweils aus Studierenden der Informatik und Studierenden der Kunstgeschichte zusammensetzt. Ein kontinuierlicher Austausch zwischen den Studierenden der unterschiedlichen Disziplinen ist essenziell; d. h. informatische Inhalte sind in den jeweiligen Gruppen von dem Studierenden der Informatik zu vermitteln, kunsthistorische Konzepte von den Studierenden der Kunstgeschichte. Wir empfehlen den Studierenden der Kunstgeschichte zudem, zur selbstständigen Exploration der Datensätze auf das Online-Tool [Museum Analytics (MAX)](https://www.max.gwi.uni-muenchen.de/) zurückzugreifen.

Das Seminar wird vollständig online abgehalten: Die [einzelnen Sitzungen](https://git.dhvlab.org/kunstgeschichte-als-datenwissenschaft/organisation/-/blob/master/seminarplan.md) finden auf [Zoom](https://lmu-munich.zoom.us/) statt, für das Projektmanagement wird [GitLab](https://git.dhvlab.org/kunstgeschichte-als-datenwissenschaft) und für die -kommunikation [Mattermost](https://teams.dhvlab.org/kg-als-dw-ss-21/) eingesetzt. Zur Vorbereitung sei auf die jeweiligen Tutorials verwiesen: [Zoom](https://support.zoom.us/hc/de/categories/200101697), [GitLab](https://docs.gitlab.com/), [Mattermost](https://docs.mattermost.com/guides/user.html). Alle Teilnehmer und Teilnehmerinnen haben sich im [DHVLab](https://dhvlab.gwi.uni-muenchen.de/mgmt/labuser/signup) anzumelden (*Lab*: „KG als DW SS 21“).


## Kontakt

Hubertus Kohle ([Website](https://www.kunstgeschichte.uni-muenchen.de/personen/professoren_innen/kohle/index.html)), Stefanie Schneider ([Website](https://www.kunstgeschichte.uni-muenchen.de/personen/wiss_ma/schneider/index.html)), François Bry ([Website](https://www.pms.ifi.lmu.de/mitarbeiter/derzeitige/francois-bry/index.html)).


## Literatur

* [Bry, François und Christoph Wieser (2012): „Squaring and Scripting the ESP Game: Trimming a GWAP to Deep Semantics“, in: *Proceedings of the International Conference on Serious Games Development and Applications*.](http://www.en.pms.ifi.lmu.de/publications/PMS-FB/PMS-FB-2012-10/PMS-FB-2012-10-paper.pdf)

* [Glinka, Katrin, Christopher Pietsch und Marian Dörk (2017): „Past Visions and Reconciling Views. Visualizing Time, Texture and Themes in Cultural Collections“, in: *Digital Humanities Quarterly* 11.2.](http://www.digitalhumanities.org/dhq/vol/11/2/000290/000290.html)

* Jannidis, Fotis, Hubertus Kohle und Malte Rehbein (Hrsg., 2017): „Digital Humanities. Eine Einführung“, Stuttgart: J. B. Metzler.

* Karsdorp, Folgert, Mike Kestemont und Allen Riddell (2021): „Humanities Data Analysis. Case Studies With Python“, Princeton: Princeton University Press.

* [Schneider, Stefanie und Hubertus Kohle (2017): „The Computer as Filter Machine: A Clustering Approach to Categorize Artworks Based on a Social Tagging Network“, in: *Artl\@s Bulletin* 6.2.](https://docs.lib.purdue.edu/cgi/viewcontent.cgi?article=1141&context=artlas)

* Suckale, Robert (2005): „Geschichte der Kunst in Deutschland“, Köln: Dumont.

* van de Waal, Henri (1973–1985): „Iconclass: An Iconographic Classification System. Completed and Edited by L. D. Couprie with R. H. Fuchs“, Amsterdam: North-Holland Publishing Company.

* van Straten, Roelof (1994): „Iconography, Indexing, Iconclass. A Handbook“, Leiden: Foleor Publishers.

* [Wieser, Christoph, François Bry, Alexandre Bérard und Richard Lagrange (2017): „ARTigo: Building an Artwork Search Engine With Games and Higher-Order Latent Semantic Analysis“, in: *Workshop on Human Computation and Machine Learning in Games*.](https://www.en.pms.ifi.lmu.de/publications/PMS-FB/PMS-FB-2013-3/PMS-FB-2013-3-paper.pdf)

* [Windhager, Florian, Paolo Federico, Eva Mayr, Günther Schreder und Michael Smuc (2016): „A Review of Information Visualization Approaches and Interfaces to Digital Cultural Heritage Collections“.](https://publik.tuwien.ac.at/files/publik_257936.pdf)
