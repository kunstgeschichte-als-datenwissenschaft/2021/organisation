 1. *14.04*: Einführung (kurze Vorstellung der Seminarleiter und -teilnehmer), Erläuterung der unterschiedlichen Datensätze, Vergabe der Projektthemen, Leistungsnachweise.
 2. *21.04*: Gastvortrag: Maria Effinger, Universitätsbibliothek Heidelberg.
 3. *28.04*: Gastvorträge (im Rahmen der Tagung: „Das digitale Bild – Die soziale Dimension, politische Perspektiven und ökonomische Zwänge“): Yvonne Zindel, Universität der Künste Berlin, und Kerstin Schankweiler, Technische Universiät Dresden.
 4. *05.05*: Gastvortrag: Ralph Knickmeier, Österreichische Galerie Belvedere.
 5. *12.05*: Abgabe und Diskussion des Arbeitspapiers (Forschungsfrage und -stand, Aufgabenverteilung, Zeitplan).
 6. *19.05*: Puffer.
 7. *26.05*: Freie Sitzung zur individuellen Betreuung der Gruppen.
 8. *02.06*: Zwischenpräsentationen (Gruppe 1 und 3, nicht öffentlich): 10 bis 15 Minuten Vortrag plus Diskussion (insgesamt maximal 45 Minuten).
 9. *09.06*: Zwischenpräsentationen (Gruppe 2 und 4, nicht öffentlich): 10 bis 15 Minuten Vortrag plus Diskussion (insgesamt maximal 45 Minuten).
10. *16.06*: Freie Sitzung zur individuellen Betreuung der Gruppen.
11. *23.06*: Freie Sitzung zur individuellen Betreuung der Gruppen.
12. *30.06*: Endpräsentationen (Gruppe 1 und 3, öffentlich): 20 bis 25 Minuten Vortrag plus Diskussion (insgesamt maximal 45 Minuten).
13. *07.07*: Endpräsentationen (Gruppe 2 und 4, öffentlich): 20 bis 25 Minuten Vortrag plus Diskussion (insgesamt maximal 45 Minuten).
14. *14.07*: Abschlussdiskussion, Fazit, Feedback und Evaluation.
